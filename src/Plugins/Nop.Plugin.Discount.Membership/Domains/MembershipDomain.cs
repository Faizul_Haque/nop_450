﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Discount.Membership.Domains
{
    public class MembershipDomain : BaseEntity
    {
        public int CustomerRoleId { get; set; }
        public string CustomerRoleName { get; set; }
        public bool IsActive { get; set; }
        public int TargetOrderCount { get; set; }
        public bool SelectTargetTotalPrice { get; set; }
        public decimal TargetTotalPrice { get; set; }
    }
}
