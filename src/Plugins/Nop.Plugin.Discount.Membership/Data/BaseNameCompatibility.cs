﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data.Mapping;
using Nop.Plugin.Discount.Membership.Domains;

namespace Nop.Plugin.Discount.Membership.Data
{
    public class BaseNameCompatibility : INameCompatibility
    {
        public Dictionary<Type, string> TableNames => 
            new Dictionary<Type, string>
            {
                {typeof(MembershipDomain), "NS_DM_Membership"}
            };

        public Dictionary<(Type, string), string> ColumnName => new Dictionary<(Type, string), string>();
    }
}
