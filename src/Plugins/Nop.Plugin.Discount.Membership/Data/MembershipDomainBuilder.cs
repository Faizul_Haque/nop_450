﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator.Builders.Create.Table;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Discount.Membership.Domains;

namespace Nop.Plugin.Discount.Membership.Data
{
    public class MembershipDomainBuilder : NopEntityBuilder<MembershipDomain>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table.WithColumn(nameof(MembershipDomain.CustomerRoleId)).AsInt32().Unique();
        }
    }
}
