﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;
using Nop.Data.Extensions;
using Nop.Data.Migrations;
using Nop.Plugin.Discount.Membership.Domains;

namespace Nop.Plugin.Discount.Membership.Data
{
    [NopMigration("2021/01/03 01:00:00", "Discount.Membership base schema", MigrationProcessType.Installation)]
    public class SchemaMigration : AutoReversingMigration
    {
        public override void Up()
        {
            Create.TableFor<MembershipDomain>();
        }
    }
}
