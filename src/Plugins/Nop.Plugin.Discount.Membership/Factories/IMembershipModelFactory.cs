﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Discount.Membership.Models;

namespace Nop.Plugin.Discount.Membership.Factories
{
    public interface IMembershipModelFactory
    {
        MembershipSearchModel PrepareMembershipSearchModel(MembershipSearchModel searchModel);
        Task<MembershipModel> PrepareMembershipModelAsync(int id);
        MembershipModel PrepareMembershipModelAsync(MembershipModel model, List<CustomerRole> customerRoles);
        Task<MembershipListModel> PrepareMembershipListModelAsync(MembershipSearchModel searchModel);
    }
}
