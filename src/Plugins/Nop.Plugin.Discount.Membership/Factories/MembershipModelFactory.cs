﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Discount.Membership.Models;
using Nop.Plugin.Discount.Membership.Services;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Discount.Membership.Factories
{
    public class MembershipModelFactory : IMembershipModelFactory
    {
        private readonly IMembershipConfigurationService _membershipConfigurationService;

        public MembershipModelFactory(IMembershipConfigurationService membershipConfigurationService)
        {
            _membershipConfigurationService = membershipConfigurationService;
        }
        public MembershipSearchModel PrepareMembershipSearchModel(MembershipSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        public async Task<MembershipModel> PrepareMembershipModelAsync(int id)
        {
            var domain = await _membershipConfigurationService.GetMembershipByIdAsync(id);
            if (domain == null)
            {
                return null;
            }
            var model = domain.ToModel<MembershipModel>();

            return model;
        }

        public MembershipModel PrepareMembershipModelAsync(MembershipModel model, List<CustomerRole> customerRoles)
        {
            model.CustomerRoles = new SelectList(customerRoles, "CustomerRoleId", "CustomerRoleName");

            return model;
        }

        public async Task<MembershipListModel> PrepareMembershipListModelAsync(MembershipSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            var roles = await _membershipConfigurationService.GetPagedAllMembershipsAsync(roleName:searchModel.CustomerRoleName);

            var model = await new MembershipListModel().PrepareToGridAsync(searchModel, roles, () =>
            {
                return roles.SelectAwait(async role =>
                {

                    //fill in model values from the entity
                    var membershipModel = role.ToModel<MembershipModel>();

                    return membershipModel;
                });
            });

            return model;
        }
    }
}
