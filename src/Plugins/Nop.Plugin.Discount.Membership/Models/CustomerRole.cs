﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Discount.Membership.Models
{
    public class CustomerRole
    {
        public int CustomerRoleId { get; set; }
        public string CustomerRoleName { get; set; }
    }
}
