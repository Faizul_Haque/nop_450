﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Discount.Membership.Models
{
    public record MembershipModel : BaseNopEntityModel
    {
        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.Id")]
        public override int Id { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.CustomerRole")]
        public int CustomerRoleId { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.CustomerRole")]
        public string CustomerRoleName { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.IsActive")]
        public bool IsActive { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.TargetOrderCount")]
        public int TargetOrderCount { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.SelectTargetTotalPrice")]
        public bool SelectTargetTotalPrice { get; set; }

        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.TargetTotalPrice")]
        public decimal TargetTotalPrice { get; set; }

        public SelectList CustomerRoles { get; set; }
    }
}
