﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Discount.Membership.Models
{
    public record MembershipSearchModel : BaseSearchModel
    {
        [NopResourceDisplayName("Plugins.Discount.Membership.Fields.CustomerRole")]
        [UIHint("CustomerRole")]
        public string CustomerRoleName { get; set; }
    }
}
