﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using Nop.Plugin.Discount.Membership.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Discount.Membership.Validators
{
    public class MembershipModelValidator : BaseNopValidator<MembershipModel>
    {
        public MembershipModelValidator(ILocalizationService localizationService)
        {
            RuleFor(model => model.TargetTotalPrice)
                .GreaterThan(0).When(model => model.SelectTargetTotalPrice)
                .WithMessageAwait(localizationService.GetResourceAsync("Plugins.Discount.Membership.Fields.TargetTotalPrice.GreaterZero"));
            
            RuleFor(model => model.TargetOrderCount)
                .GreaterThan(0).When(model => model.SelectTargetTotalPrice == false)
                .WithMessageAwait(localizationService.GetResourceAsync("Plugins.Discount.Membership.Fields.TargetOrderCount.GreaterZero"));
        }
    }
}
