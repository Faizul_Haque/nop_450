﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Discount.Membership.Factories;
using Nop.Plugin.Discount.Membership.Services;

namespace Nop.Plugin.Discount.Membership.Infrastructure
{
    public class NopStartUp : INopStartup
    {
        public int Order => 100;

        public void Configure(IApplicationBuilder application)
        {
          
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IMembershipConfigurationService, MembershipConfigurationService>();

            services.AddScoped<IMembershipModelFactory, MembershipModelFactory>();
        }
    }
}
