﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Discount.Membership.Domains;
using Nop.Plugin.Discount.Membership.Models;

namespace Nop.Plugin.Discount.Membership.Infrastructure
{
    public class MapperConfiguration : Profile, IOrderedMapperProfile
    {
        public MapperConfiguration()
        {
            CreateMap<MembershipDomain, MembershipModel>();

            CreateMap<MembershipModel, MembershipDomain>()
                .ForMember(model => model.Id, options => options.Ignore());

            //CreateMap<List<DiscountMembershipConfigurationDomain>, List<DiscountMembershipConfigurationModel>>();
        }
        public int Order => 1;
    }
}
