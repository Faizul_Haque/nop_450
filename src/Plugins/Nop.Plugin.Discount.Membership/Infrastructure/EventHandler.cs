﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Discount.Membership.Services;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Orders;

namespace Nop.Plugin.Discount.Membership.Infrastructure
{
    public class EventHandler : IConsumer<OrderPaidEvent>
    {
        private readonly ICustomerService _customerService;
        private readonly IOrderService _orderService;
        private readonly IMembershipConfigurationService _membershipConfigurationService;

        public EventHandler(ICustomerService customerService, IOrderService orderService, IMembershipConfigurationService membershipConfigurationService)
        {
            _customerService = customerService;
            _orderService = orderService;
            _membershipConfigurationService = membershipConfigurationService;
        }
        public async Task HandleEventAsync(OrderPaidEvent eventMessage)
        {
            var order = eventMessage.Order;
            var customer = await _customerService.GetCustomerByIdAsync(order.CustomerId);

            var allOrders = await _orderService.SearchOrdersAsync(customerId: customer.Id);

            //_orderService.

            var allCustomerRoles = (await _customerService.GetCustomerRoleIdsAsync(customer)).ToList();

            var orderCount = allOrders.Count();

            decimal totalSumOfMoneyOrdered = 0;
            foreach (var x in allOrders)
            {
                totalSumOfMoneyOrdered += x.OrderTotal;
            }

            var memberships = await _membershipConfigurationService.GetAllMembershipsAsync();

            foreach(var membership in memberships)
            {
                var role = await _customerService.GetCustomerRoleByIdAsync(membership.CustomerRoleId);
                if(await _customerService.IsInCustomerRoleAsync(customer, role.SystemName))
                {
                    continue;
                }

                //if (allCustomerRoles.Contains(membership.CustomerRoleId))
                //{
                //    continue;
                //}
                bool toAdd = false;
                if(membership.SelectTargetTotalPrice)
                {
                    if(membership.TargetTotalPrice <= totalSumOfMoneyOrdered)
                    {
                        toAdd = true;
                    }
                }
                else
                {
                    if(membership.TargetOrderCount <= orderCount)
                    {
                        toAdd = true;
                    }
                }
                if(toAdd)
                {
                    await _customerService.AddCustomerRoleMappingAsync(new CustomerCustomerRoleMapping { CustomerId = customer.Id, CustomerRoleId = membership.CustomerRoleId });
                }
            }
        }
    }
}
