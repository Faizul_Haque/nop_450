﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Data;
using Nop.Plugin.Discount.Membership.Domains;
using Nop.Plugin.Discount.Membership.Models;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;

namespace Nop.Plugin.Discount.Membership.Services
{
    public class MembershipConfigurationService : IMembershipConfigurationService
    {
        private readonly IRepository<MembershipDomain> _repoConfig;

        public MembershipConfigurationService(IRepository<MembershipDomain> repoConfig)
        {
            _repoConfig = repoConfig;
        }

        public async Task<bool> CreateNewMembershipAsync(MembershipModel model)
        {
            var domain = model.ToEntity<MembershipDomain>();

            try
            {
                await _repoConfig.InsertAsync(domain);
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<List<MembershipDomain>> GetAllMembershipsAsync()
        {
            var domain = (await _repoConfig.GetAllAsync(query => query.Where(x => x.Id > 0))).ToList();

            return domain;
        }

        public async Task<IPagedList<MembershipDomain>> GetPagedAllMembershipsAsync(string roleName=null)
        {
            var model = await _repoConfig.Table.ToPagedListAsync(pageIndex: 0, pageSize: int.MaxValue);

            if (roleName != null)
            {
                model = await _repoConfig.Table.Where(x => x.CustomerRoleName == roleName).ToPagedListAsync(pageIndex: 0, pageSize: int.MaxValue);
            }
            return model;
        }

        public async Task<MembershipDomain> GetMembershipByIdAsync(int id)
        {
            var domain = await _repoConfig.Table.Where(x => x.Id == id).FirstOrDefaultAsync();

            return domain;
        }

        public async Task<MembershipModel> GetMembershipByRoleIdAsync(int roleId)
        {
            var domain = await _repoConfig.Table.Where(x => x.CustomerRoleId == roleId).FirstOrDefaultAsync();

            if(domain == null)
            {
                return null;
            }

            var model = domain.ToModel<MembershipModel>();

            return model;
        }
        public async Task DeleteMembershipAsync(int id)
        {
            var domain = await GetMembershipByIdAsync(id);

            try
            {
                await _repoConfig.DeleteAsync(domain);
            }
            catch (Exception ex)
            {
                throw new ArgumentNullException(nameof(domain));
            }
        }

        public async Task DeleteMembershipsAsync(List<int> ids)
        {
            if (ids == null || ids.Count == 0)
            {
                throw new ArgumentNullException(nameof(ids));
            }
            foreach (var id in ids)
            {
                await DeleteMembershipAsync(id);
            }
        }

        public async Task UpdateMembershipAsync(MembershipModel model)
        {
            var domain = await GetMembershipByIdAsync(model.Id);
            //domain = model.ToEntity<MembershipDomain>();
            domain = model.ToEntity(domain);

            await _repoConfig.UpdateAsync(domain);
        }
    }
}
