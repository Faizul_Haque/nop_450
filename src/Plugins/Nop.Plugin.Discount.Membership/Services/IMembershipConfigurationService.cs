﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Discount.Membership.Domains;
using Nop.Plugin.Discount.Membership.Models;

namespace Nop.Plugin.Discount.Membership.Services
{
    public interface IMembershipConfigurationService
    {
        Task<bool> CreateNewMembershipAsync(MembershipModel model);
        Task<List<MembershipDomain>> GetAllMembershipsAsync();
        Task<IPagedList<MembershipDomain>> GetPagedAllMembershipsAsync(string roleName = null);
        Task<MembershipDomain> GetMembershipByIdAsync(int id);
        Task<MembershipModel> GetMembershipByRoleIdAsync(int roleId);
        Task DeleteMembershipAsync(int id);
        Task DeleteMembershipsAsync(List<int> ids);
        Task UpdateMembershipAsync(MembershipModel model);
    }
}