﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Discount.Membership.Models;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.Discount.Membership.Services;
using Nop.Services.Customers;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Discount.Membership.Factories;
using Nop.Services.Messages;
using Nop.Services.Localization;

namespace Nop.Plugin.Discount.Membership.Controllers
{
    public class MembershipController : BaseAdminController //BasePluginController
    {
        private readonly IMembershipConfigurationService _membershipConfigurationService = null;
        private readonly ICustomerService _customerService;
        private readonly IMembershipModelFactory _membershipModelFactory;
        private readonly INotificationService _notificationService;
        private readonly ILocalizationService _localizationService;

        public MembershipController(IMembershipConfigurationService membershipConfigurationService, ICustomerService customerService, IMembershipModelFactory membershipModelFactory, INotificationService notificationService, ILocalizationService localizationService)
        {
            _membershipConfigurationService = membershipConfigurationService;
            _customerService = customerService;
            _membershipModelFactory = membershipModelFactory;
            _notificationService = notificationService;
            _localizationService = localizationService;
        }
        public IActionResult Configure()
        {
            var model = _membershipModelFactory.PrepareMembershipSearchModel(new MembershipSearchModel());

            return View("~/Plugins/Discount.Membership/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> Configure(MembershipSearchModel searchModel)
        {
            var model = await _membershipModelFactory.PrepareMembershipListModelAsync(searchModel);
            return Json(model);
        }
        
        public async Task<IActionResult> Create()
        {
            var createModel = new MembershipModel();
            var customerRoles = (await _customerService.GetAllCustomerRolesAsync()).Select(x => new CustomerRole()
            {
                CustomerRoleId = x.Id,
                CustomerRoleName = x.Name,
            }).ToList();

            createModel = _membershipModelFactory.PrepareMembershipModelAsync(createModel, customerRoles);

            return View("~/Plugins/Discount.Membership/Views/Create.cshtml", createModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(MembershipModel createModel)
        {
            if (ModelState.IsValid)
            {
                var customerRoleName = await _customerService.GetCustomerRoleByIdAsync(createModel.CustomerRoleId);

                createModel.CustomerRoleName = customerRoleName.Name;

                bool result = await _membershipConfigurationService.CreateNewMembershipAsync(createModel);

                if (!result)
                {
                    var customerRolesLocal = (await _customerService.GetAllCustomerRolesAsync()).Select(x => new CustomerRole()
                    {
                        CustomerRoleId = x.Id,
                        CustomerRoleName = x.Name,
                    }).ToList();

                    createModel = _membershipModelFactory.PrepareMembershipModelAsync(createModel, customerRolesLocal);
                    
                    ModelState.AddModelError("CustomerRoleId", "This role already exist!!");
                    return View("~/Plugins/Discount.Membership/Views/Create.cshtml", createModel);
                }
                return RedirectToAction("Configure");
            }

            var customerRoles = (await _customerService.GetAllCustomerRolesAsync()).Select(x => new CustomerRole()
            {
                CustomerRoleId = x.Id,
                CustomerRoleName = x.Name,
            }).ToList();

            createModel = _membershipModelFactory.PrepareMembershipModelAsync(createModel, customerRoles);

            return View("~/Plugins/Discount.Membership/Views/Create.cshtml", createModel);
        }


        public async Task<IActionResult> Edit(int id)
        {
            var model = await _membershipModelFactory.PrepareMembershipModelAsync(id);

            return View("~/Plugins/Discount.Membership/Views/Edit.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(int id, MembershipModel model)
        {
            if (ModelState.IsValid)
            {
                if(model.SelectTargetTotalPrice)
                {
                    model.TargetOrderCount = 0;
                }
                else
                {
                    model.TargetTotalPrice = 0;
                }
                await _membershipConfigurationService.UpdateMembershipAsync(model);
            }
            return RedirectToAction("Configure");
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {
            await _membershipConfigurationService.DeleteMembershipAsync(id);

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Catalog.Categories.Deleted"));

            return RedirectToAction("Configure");
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSelected(ICollection<int> selectedIds)
        {
            if (selectedIds == null || selectedIds.Count == 0)
                return NoContent();

            await _membershipConfigurationService.DeleteMembershipsAsync(selectedIds.ToList());

            return Json(new { Result = true });
        }
    }
}
