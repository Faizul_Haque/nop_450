﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Discount.Membership.Factories;
using Nop.Plugin.Discount.Membership.Models;
using Nop.Plugin.Discount.Membership.Services;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Discount.Membership.Components
{
    public class WidgetsMembershipViewComponent : NopViewComponent
    {
        private readonly IMembershipConfigurationService _membershipConfigurationService = null;
        private readonly IMembershipModelFactory _membershipModelFactory;
        public WidgetsMembershipViewComponent(IMembershipConfigurationService membershipConfigurationService, IMembershipModelFactory membershipModelFactory)
        {
            _membershipConfigurationService = membershipConfigurationService;
            _membershipModelFactory = membershipModelFactory;
        }

        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            var model = new MembershipModel();

            if (additionalData != null)
            {
                int roleId = additionalData is CustomerRoleModel x ? x.Id : 0;
                string roleName = additionalData is CustomerRoleModel y ? y.Name : "";
                if (roleId != 0)
                {
                    model = await _membershipConfigurationService.GetMembershipByRoleIdAsync(roleId);
                    if (model == null)
                    {
                        model = new MembershipModel();
                    }
                }
                model.CustomerRoleId = roleId;
                model.CustomerRoleName = roleName;
            }
            return View("~/Plugins/Discount.Membership/Views/PublicInfo.cshtml",model);
        }
    }
}
