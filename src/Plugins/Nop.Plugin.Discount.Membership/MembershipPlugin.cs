﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Services.Cms;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Discount.Membership
{
    public class MembershipPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;

        public MembershipPlugin(IWebHelper webHelper, ILocalizationService localizationService)
        {
            _webHelper = webHelper;
            _localizationService = localizationService;
        }

        public bool HideInWidgetList => false;

        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/Membership/Configure";
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsMembership";
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { AdminWidgetZones.CustomerRoleDetailsBottom });
        }

        public override async Task InstallAsync()
        {
            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Discount.Membership.Fields.Id"] = "Id",
                ["Plugins.Discount.Membership.Fields.CustomerRole"] = "Membership Name",
                ["Plugins.Discount.Membership.Fields.CustomerRole.Hint"] = "A Membership Name",

                ["Plugins.Discount.Membership.Fields.IsActive"] = "Active",
                ["Plugins.Discount.Membership.Fields.IsActive.Hint"] = "Whether it is Activated",

                ["Plugins.Discount.Membership.Fields.TargetOrderCount"] = "Order Count",
                ["Plugins.Discount.Membership.Fields.TargetOrderCount.Hint"] = "Number of orders needed",

                ["Plugins.Discount.Membership.Fields.SelectTargetTotalPrice"] = "Select in Price",
                ["Plugins.Discount.Membership.Fields.SelectTargetTotalPrice.Hint"] = "Select for using Target Price",

                ["Plugins.Discount.Membership.Fields.TargetTotalPrice"] = "Total Price",
                ["Plugins.Discount.Membership.Fields.TargetTotalPrice.Hint"] = "Target Price",

                ["Plugins.Discount.Membership.Name"] = "Membership",
                ["Plugins.Discount.Membership.BackToList"] = "Back to Membership List",

                ["Plugins.Discount.Membership.Fields.TargetOrderCount.GreaterZero"] = "Number of orders must be greater than 0",
                ["Plugins.Discount.Membership.Fields.TargetTotalPrice.GreaterZero"] = "Required total price must be greater than 0",

            });
            await base.InstallAsync();
        }

        public override async Task UpdateAsync(string currentVersion, string targetVersion)
        {
            if(currentVersion == "1.51" && targetVersion == "1.52")
            {
                await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
                {
                    ["Plugins.Discount.Membership.Fields.Id"] = "Id",
                    ["Plugins.Discount.Membership.Fields.CustomerRole"] = "Membership Name",
                    ["Plugins.Discount.Membership.Fields.CustomerRole.Hint"] = "A Membership Name",

                    ["Plugins.Discount.Membership.Fields.IsActive"] = "Active",
                    ["Plugins.Discount.Membership.Fields.IsActive.Hint"] = "Whether it is Activated",

                    ["Plugins.Discount.Membership.Fields.TargetOrderCount"] = "Order Count",
                    ["Plugins.Discount.Membership.Fields.TargetOrderCount.Hint"] = "Number of orders needed",

                    ["Plugins.Discount.Membership.Fields.SelectTargetTotalPrice"] = "Select in Price",
                    ["Plugins.Discount.Membership.Fields.SelectTargetTotalPrice.Hint"] = "Select for using Target Price",

                    ["Plugins.Discount.Membership.Fields.TargetTotalPrice"] = "Total Price",
                    ["Plugins.Discount.Membership.Fields.TargetTotalPrice.Hint"] = "Target Price",

                    ["Plugins.Discount.Membership.Name"] = "Membership",
                    ["Plugins.Discount.Membership.BackToList"] = "Back to Membership List",

                    ["Plugins.Discount.Membership.Fields.TargetOrderCount.GreaterZero"] = "Number of orders must be greater than 0",
                    ["Plugins.Discount.Membership.Fields.TargetTotalPrice.GreaterZero"] = "Required total price must be greater than 0",
                });
            }
            await base.UpdateAsync(currentVersion, targetVersion);
        }
    }
}