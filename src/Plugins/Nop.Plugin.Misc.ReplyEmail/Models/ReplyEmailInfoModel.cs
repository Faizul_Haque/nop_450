﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Misc.ReplyEmail.Models
{
    public record ReplyEmailInfoModel : BaseNopModel
    {
        public int MessageTemplateId { get; set; }

        [NopResourceDisplayName("Plugins.Misc.ReplyEmail.ReplyTo")]
        public string ReplyTo { get; set; }
        public SelectList AllowedTokens { get; set; }
    }
}
