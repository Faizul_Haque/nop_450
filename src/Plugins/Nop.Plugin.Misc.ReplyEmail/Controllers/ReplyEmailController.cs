﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Misc.ReplyEmail.Models;
using Nop.Services.Common;
using Nop.Services.Messages;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Misc.ReplyEmail.Controllers
{
    public class ReplyEmailController : BaseAdminController
    {
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IGenericAttributeService _genericAttributeService;
        public ReplyEmailController(IMessageTemplateService messageTemplateService, IGenericAttributeService genericAttributeService)
        {
            _messageTemplateService = messageTemplateService;
            _genericAttributeService = genericAttributeService;
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ReplyEmailInfoModel model)
        {
            if (ModelState.IsValid)
            {
                var message = await _messageTemplateService.GetMessageTemplateByIdAsync(model.MessageTemplateId);
                if (message != null)
                {
                    await _genericAttributeService.SaveAttributeAsync(message, ReplyEmailDefaults.ReplyToEmail, model.ReplyTo);
                }
            }
            else
            {
                return ErrorJson(ModelState.SerializeErrors());
            }

            return Json(new { Result = true });
        }
    }
}
