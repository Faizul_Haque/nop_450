﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Misc.ReplyEmail.Models;
using Nop.Services.Common;
using Nop.Services.Messages;
using Nop.Web.Areas.Admin.Models.Messages;
using Nop.Web.Framework.Components;


namespace Nop.Plugin.Misc.ReplyEmail.Components
{
    public class ReplyEmailViewComponent : NopViewComponent
    {
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IMessageTokenProvider _messageTokenProvider;

        public ReplyEmailViewComponent(IMessageTemplateService messageTemplateService, IGenericAttributeService genericAttributeService, IMessageTokenProvider messageTokenProvider)
        {
            _messageTemplateService = messageTemplateService;
            _genericAttributeService = genericAttributeService;
            _messageTokenProvider = messageTokenProvider;
        }
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            var model = new ReplyEmailInfoModel();

            if(additionalData != null)
            {
                var messageTemplateId = additionalData is MessageTemplateModel x ? x.Id : -1;
                if(messageTemplateId != -1)
                {
                    var message = await _messageTemplateService.GetMessageTemplateByIdAsync(messageTemplateId);

                    var tokensGroups = _messageTokenProvider.GetTokenGroups(message);

                    var allowedTokens = (await _messageTokenProvider.GetListOfAllowedTokensAsync(tokensGroups)).Where(x => x.Contains("Email%")).ToList();

                    model.MessageTemplateId = messageTemplateId;
                    model.ReplyTo = await _genericAttributeService.GetAttributeAsync<string>(message, ReplyEmailDefaults.ReplyToEmail);
                    model.AllowedTokens = new SelectList(allowedTokens);
                }
            }
            return View("~/Plugins/Misc.ReplyEmail/Views/ReplyEmailInfo.cshtml", model);
        }
    }
}
