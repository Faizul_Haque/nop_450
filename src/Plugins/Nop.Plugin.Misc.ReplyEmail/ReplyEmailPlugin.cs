﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Services.Cms;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Misc.ReplyEmail
{
    public class ReplyEmailPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ILocalizationService _localizationService;
        public ReplyEmailPlugin(ILocalizationService localizationService)
        {
            _localizationService = localizationService;
        }
        public bool HideInWidgetList => false;

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "ReplyEmail";
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { AdminWidgetZones.MessageTemplateDetailsBottom });
        }

        public async override Task InstallAsync()
        {
            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Misc.ReplyEmail.ReplyTo"] = "Reply-To",
                ["Plugins.Misc.ReplyEmail.ReplyTo.Hint"] = "The Email Account that will be used for replying this Email - You should use token here."
            });
            await base.InstallAsync();
        }
    }
}