﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Misc.ReplyEmail.Services;
using Nop.Services.Messages;

namespace Nop.Plugin.Misc.ReplyEmail.Infrastructure
{
    public class NopStartup : INopStartup
    {
        public int Order => 4000;

        public void Configure(IApplicationBuilder application)
        {
            
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IWorkflowMessageService, ReplyEmailMessageService>();
        }
    }
}
