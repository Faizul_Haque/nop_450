﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.HelloWorld.Services;
using Nop.Web.Framework.Components;

namespace Nop.Plugin.Widgets.HelloWorld.Components
{
    public class HelloFaizulViewComponent : NopViewComponent
    {
        private readonly IConfigurationService _configService;

        public HelloFaizulViewComponent(IConfigurationService configService)
        {
            _configService = configService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var viewModel = await _configService.GetData();
            return View("~/Plugins/Widgets.HelloWorld/Views/PublicInfo.cshtml", viewModel);
        }
    }
}
