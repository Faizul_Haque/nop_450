﻿using System.Threading.Tasks;
using Nop.Plugin.Widgets.HelloWorld.Models;

namespace Nop.Plugin.Widgets.HelloWorld.Services
{
    public interface IConfigurationService
    {
        Task SaveData(ConfigurationModel viewModel);
        Task<PublicInfoModel> GetData();
    }
}