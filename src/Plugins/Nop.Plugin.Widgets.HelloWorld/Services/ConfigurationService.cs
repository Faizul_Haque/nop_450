﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Data;
using Nop.Plugin.Widgets.HelloWorld.Domains;
using Nop.Plugin.Widgets.HelloWorld.Models;

namespace Nop.Plugin.Widgets.HelloWorld.Services
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IRepository<ConfigurationDomain> _repoConfig;

        public ConfigurationService(IRepository<ConfigurationDomain> repoConfig)
        {
            _repoConfig = repoConfig;
        }
        public async Task SaveData(ConfigurationModel viewModel)
        {
            var model = new ConfigurationDomain()
            {
                Name = viewModel.Name,
            };

            await _repoConfig.InsertAsync(model);
        }
        public async Task<PublicInfoModel> GetData()
        {
            int id = _repoConfig.Table.Max(x => x.Id);
            var model = await _repoConfig.GetByIdAsync(id);
            var viewModel = new PublicInfoModel()
            {
                Name = model.Name
            };
            return viewModel;
        }
    }
}
