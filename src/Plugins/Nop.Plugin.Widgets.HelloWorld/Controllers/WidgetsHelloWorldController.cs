﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Plugin.Widgets.HelloWorld.Models;
using Nop.Plugin.Widgets.HelloWorld.Services;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.HelloWorld.Controllers
{
    public class WidgetsHelloWorldController : BaseAdminController
    {
        private readonly IConfigurationService _configService;

        public WidgetsHelloWorldController(IConfigurationService configService)
        {
            _configService = configService;
        }
        public IActionResult Configure()
        {
            var model = new ConfigurationModel();
            return View("~/Plugins/Widgets.HelloWorld/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public async Task<IActionResult> Configure(ConfigurationModel viewModel)
        {
            await _configService.SaveData(viewModel);
            return View("~/Plugins/Widgets.HelloWorld/Views/Configure.cshtml",viewModel);
        }
    }
}
