﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Factories;
using Nop.Plugin.Payments.QCardPayment.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Plugins;

namespace Nop.Plugin.Payments.QCardPayment
{
    public class QCardPaymentPlugin : BasePlugin, IPaymentMethod
    {
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly QCardPaymentSettings _qCardPaymentSettings;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IStoreContext _storeContext;
        private readonly IQCardPaymentFactory _qCardPaymentFactory;
        private readonly IOrderService _orderService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public QCardPaymentPlugin(IOrderTotalCalculationService orderTotalCalculationService, QCardPaymentSettings qCardPaymentSettings, ILocalizationService localizationService, ISettingService settingService, IWebHelper webHelper, IStoreContext storeContext, IQCardPaymentFactory qCardPaymentFactory, IOrderService orderService, IHttpContextAccessor httpContextAccessor)
        {
            _orderTotalCalculationService = orderTotalCalculationService;
            _qCardPaymentSettings = qCardPaymentSettings;
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
            _storeContext = storeContext;
            _qCardPaymentFactory = qCardPaymentFactory;
            _orderService = orderService;
            _httpContextAccessor = httpContextAccessor;
        }
        public bool SupportCapture => true;

        public bool SupportPartiallyRefund => false;

        public bool SupportRefund => false;

        public bool SupportVoid => true;

        public RecurringPaymentType RecurringPaymentType => RecurringPaymentType.NotSupported;

        public PaymentMethodType PaymentMethodType => PaymentMethodType.Redirection;

        public bool SkipPaymentInfo => false;


        public override async Task InstallAsync()
        {
            var settings = new QCardPaymentSettings()
            {
                MerchantId = "12321",
                LoginId = "203399",
                Password = "Password01",
                ApiKey = "2bef9740cd0be5995e58f9eef3249cabbd65d4bc",
                UseSandbox = true
            };
            await _settingService.SaveSettingAsync(settings);

            await _localizationService.AddOrUpdateLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Payments.QCardPayment.Fields.UseSandbox"] = "Use Sandbox",
                ["Plugins.Payments.QCardPayment.Fields.UseSandbox.Hint"] = "Check to enable Sandbox(testing environment)",
                ["Plugins.Payments.QCardPayment.Fields.MerchantId"] = "Merchant ID",
                ["Plugins.Payments.QCardPayment.Fields.MerchantId.Hint"] = "Your merchant id provided to you by Flexi Cards",
                ["Plugins.Payments.QCardPayment.Fields.LoginId"] = "Login ID",
                ["Plugins.Payments.QCardPayment.Fields.LoginId.Hint"] = "Your login id provided to you by Flexi Cards",
                ["Plugins.Payments.QCardPayment.Fields.Password"] = "Password",
                ["Plugins.Payments.QCardPayment.Fields.Password.Hint"] = "Your password provided to you by Flexi Cards",
                ["Plugins.Payments.QCardPayment.Fields.ApiKey"] = "API Key",
                ["Plugins.Payments.QCardPayment.Fields.ApiKey.Hint"] = "Key defined by Flexi Cards for this service",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFee"] = "Additional fee",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFee.Hint"] = "Enter additional fee to charge your customers.",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFeePercentage"] = "Additional fee. Use percentage",
                ["Plugins.Payments.QCardPayment.Fields.AdditionalFeePercentage.Hint"] = "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.",
                ["Plugins.Payments.QCardPayment.PaymentMethodDescription"] = "Pay with Q Card",
            });
            await base.InstallAsync();
        }
        public override async Task UninstallAsync()
        {
            await _settingService.DeleteSettingAsync<QCardPaymentSettings>();

            //locales
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Payments.QCardPayment");

            await base.UninstallAsync();
        }
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/QCardPaymentAdmin/Configure";
        }


        public Task<CancelRecurringPaymentResult> CancelRecurringPaymentAsync(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            return Task.FromResult(new CancelRecurringPaymentResult());
        }

        public Task<bool> CanRePostProcessPaymentAsync(Order order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order));
            return Task.FromResult(true);
        }

        public Task<CapturePaymentResult> CaptureAsync(CapturePaymentRequest capturePaymentRequest)
        {
            return Task.FromResult(new CapturePaymentResult { Errors = new[] { "Capture method not supported" } });
        }

        public async Task<decimal> GetAdditionalHandlingFeeAsync(IList<ShoppingCartItem> cart)
        {
            return await _orderTotalCalculationService.CalculatePaymentAdditionalFeeAsync(cart,
                _qCardPaymentSettings.AdditionalFee, _qCardPaymentSettings.AdditionalFeePercentage);

        }

        public Task<ProcessPaymentRequest> GetPaymentInfoAsync(IFormCollection form)
        {
            return Task.FromResult(new ProcessPaymentRequest());
        }

        public async Task<string> GetPaymentMethodDescriptionAsync()
        {
            return await _localizationService.GetResourceAsync("Plugins.Payments.QCardPayment.PaymentMethodDescription");
        }

        public string GetPublicViewComponentName()
        {
            return "QCardPayment";
        }

        public Task<bool> HidePaymentMethodAsync(IList<ShoppingCartItem> cart)
        {
            return Task.FromResult(false);
        }

        public async Task PostProcessPaymentAsync(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            var storeScope = (await _storeContext.GetCurrentStoreAsync()).Id;
            var settings = await _settingService.LoadSettingAsync<QCardPaymentSettings>(storeScope);

            var requestUrl = settings.UseSandbox ? QCardPaymentDefaults.PaymentUrlTestEndPoint : QCardPaymentDefaults.PaymentUrlEndPoint;

            var request = WebRequest.Create(requestUrl);
            request.Method = "POST";
            request.ContentType = "application/json";
            
            var order = postProcessPaymentRequest.Order;
            var requestBody = await _qCardPaymentFactory.PreparePaymentUrlRequest(settings, order);

            var json = JsonConvert.SerializeObject(requestBody);
            request.ContentLength = json.Length;
            using(var webStream = request.GetRequestStream())
            {
                using var requestWriter = new StreamWriter(webStream, Encoding.ASCII);
                requestWriter.Write(json);
            }
            try
            {
                var webResponse = request.GetResponse();
                using var webStream = webResponse.GetResponseStream() ?? Stream.Null;
                using var responseReader = new StreamReader(webStream);
                var response = responseReader.ReadToEnd();
                var paymentUrlResponse = JsonConvert.DeserializeObject<PaymentUrlResponse>(response);

                if(paymentUrlResponse.Result.ResultCode == QCardPaymentDefaults.ResponseSuccessful)
                {
                    var oderId = paymentUrlResponse.MerchantTransactionId;
                    var processNo = paymentUrlResponse.ProcessNo;
                    var redirectionUrl = paymentUrlResponse.PaymentUrl;
                    
                    order.AuthorizationTransactionId = processNo;
                    await _orderService.UpdateOrderAsync(order);

                    _httpContextAccessor.HttpContext.Response.Redirect(redirectionUrl);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public Task<ProcessPaymentResult> ProcessPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            return Task.FromResult(new ProcessPaymentResult());
        }

        public Task<ProcessPaymentResult> ProcessRecurringPaymentAsync(ProcessPaymentRequest processPaymentRequest)
        {
            return Task.FromResult(new ProcessPaymentResult());
        }

        public Task<RefundPaymentResult> RefundAsync(RefundPaymentRequest refundPaymentRequest)
        {
            return Task.FromResult(new RefundPaymentResult { Errors = new[] { "Refund method not supported" } });
        }

        public Task<IList<string>> ValidatePaymentFormAsync(IFormCollection form)
        {
            return Task.FromResult<IList<string>>(new List<string>());
        }

        public Task<VoidPaymentResult> VoidAsync(VoidPaymentRequest voidPaymentRequest)
        {
            return Task.FromResult(new VoidPaymentResult { Errors = new[] { "Void method not supported" } });
        }
    }
}