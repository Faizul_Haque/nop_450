﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Payments.QCardPayment.Models;

namespace Nop.Plugin.Payments.QCardPayment.Factories
{
    public interface IQCardPaymentFactory
    {
        Task<PaymentUrlRequest> PreparePaymentUrlRequest(QCardPaymentSettings settings, Order order);
        List<string> PrepareQCardProducts();
        string PrepareQCardPaymentStatusQueryString(string requestUrl, PaymentStatusRequest requestBody);
    }
}